# Application Title & small description

## Dev Dependencies
### Backend

    - pypi v3.5.2
    - Django v1.11
    - DRF Version 3

## Download and Setup

### Front End

#### Install Grunt and other deps
    $ (sudo)npm install -g grunt
    $ npm install

#### Grunt tasks
    $ cd client
    $ grunt build
    $ grunt copy

#### Live re-deploy of changes
    $ grunt watch

### Download & install virtualenv

    $ pip install virtualenv

### Create and activate the virtual environment

    $ make venv
    $ source venv/bin/activate

    - If you wish to deactivate
    $ deactivate

### Install project's development dependencies

#### Build and install python enviroment
    $ python setup.py develop

#### Install project's requirements
    $ make install

#### Setup the database and create the first django super user
    $ make setup

#### Setup the database and create the first django super user
    $ make fixtures

#### Now run the server
    $ ./manage.py runserver


- Visit http://localhost:8000
- You can login to the admin site using username: admin and password:adminpass


