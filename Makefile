clean:
	rm -f db.sqlite3

venv:
	virtualenv -p /usr/bin/python3 venv

install:
	pip install -r requirements/base.txt

migrate:
	./manage.py makemigrations --noinput
	./manage.py migrate  --noinput

superuser:
	./manage.py shell -c "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', 'adminpass')"


setup:	install migrate superuser

setup_clean: clean migrate superuser


fixtures:
	./manage.py loaddata data.json
