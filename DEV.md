# Developer's manual

## Client

#### For all events raised on $rootScope using $broadcast the following naming conventions should be followed:

myEventName-event

For example, for events that are aimed at displaying notifications the event _notifications-event_ is used as name.