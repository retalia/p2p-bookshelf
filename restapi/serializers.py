from rest_framework import serializers

from .models import Item, Asset
from django.contrib.auth.models import User


'''
class ItemSerializer(serializers.Serializer):
    # define the files that get serialized/deserialized
    name = serializers.CharField(required=False, allow_blank=True, max_length=512)
    type = serializers.CharField(required=False, allow_blank=True, max_length=512)

    def create(self, validated_data):
        """ Create and return a new Item instance """

        return Item.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """ Update and return an existing Item instance """

        instance.name = validated_data.get('name', instance.name)
        instance.type = validated_data.get('type', instance.type)
'''


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ('name', 'type')

class AssetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Asset
        fields = ('id', 'name', 'status')

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')