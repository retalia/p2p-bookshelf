from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Item(models.Model):
    name = models.CharField(max_length=512, blank=True, default='')
    type = models.CharField(max_length=16, blank=True, default='')

    def __str__(self):
        return self.name

class Asset(models.Model):
    name = models.CharField(max_length=128, blank=True, default='')
    ownerAccount = models.ForeignKey(User, related_name='owned_assets')
    holderAccount = models.ForeignKey(User, related_name='held_assets')
    status = models.CharField(max_length=20, default='OPEN')
