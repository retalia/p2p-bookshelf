from django.conf.urls import url
from restapi import views
from rest_framework import routers
from django.conf.urls import url, include

# router = routers.DefaultRouter()
# router.register(r'users', views.UserViewSet)
# router.register(r'assets', views.AssetViewSet)

urlpatterns = [
    url(r'^users$',views.UserViews.as_view()),
    url(r'^api/assets$', views.AssetListViews.as_view()),
    url(r'^api/assets/(?P<pk>[0-9a-zA-Z]+)$', views.AssetViews.as_view()),
    # url(r'^echo/?$', views.echo),
    # url(r'^', include(router.urls)),
    # url(r'^items/$', views.item_list),
    # url(r'^item/(?P<pk>[0-9]+)/$', views.item_detail),
    # #url(r'^assets/?$', views.assets_handler),
    # #url(r'^assets/([0-9]+)$', views.asset_detail),
    # #url(r'^users/([a-z]+)/requests', views.user_requests),
    url(r'^', views.root, name='root'),
]