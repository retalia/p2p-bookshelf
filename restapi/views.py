from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from .models import Item, Asset
from django.contrib.auth.models import User
from .serializers import ItemSerializer, AssetSerializer, UserSerializer
from rest_framework import viewsets, status
# for html views
from django.shortcuts import render
import logging

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from rest_framework.permissions import IsAuthenticated
from django.http import Http404


logger = logging.getLogger('bookshelf')


class UserViews(APIView):
    """
    View to list all users in the system.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    #authentication_classes = (authentication.TokenAuthentication,)
    #permission_classes = (permissions.IsAdminUser,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        """
        Return a list of all users.
        """
        usernames = [user.username for user in User.objects.all()]
        return Response(usernames)

class AssetListViews(APIView):
    """
    REST operations for asset listings i.e. put new asset in the list and retrieve asset list.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    #authentication_classes = (authentication.TokenAuthentication,)
    #permission_classes = (permissions.IsAdminUser,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        """
        Return a list of all users.
        """
        assets = [asset for asset in Asset.objects.all()]
        serializer = AssetSerializer(assets, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        logger.error("creating asset")
        name = request.data.get('name', '')
        asset_status = request.data.get('status', 'OPEN')
        newAsset = Asset.objects.create(name=name, status=asset_status, ownerAccount=request.user, holderAccount=request.user)
        serializer = AssetSerializer(newAsset)

        return Response(serializer.data, status=status.HTTP_201_CREATED)

class AssetViews(APIView):


    permission_classes = (IsAuthenticated,)
    def get(self, request, pk, format=None):
        logger.debug("retrieving asset %s", pk)
        try:
            asset = Asset.objects.get(pk=pk)
        except Asset.DoesNotExist:
            raise Http404
        serializer = AssetSerializer(asset)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def delete(self, request, pk, format=None):
        logger.debug("removing asset %s", pk)
        try:
            asset = Asset.objects.get(pk=pk)
            asset.delete()
        except Asset.DoesNotExist:
            raise Http404

        return Response(status=status.HTTP_204_NO_CONTENT)

    def put(self, request, pk, format=None):
        logger.debug("updating asset %s", pk)
        try:
            asset = Asset.objects.get(pk=pk)
            # are we updating 'status' ?
            newStatus = request.data.get('status',None)
            if newStatus is not None:
                if request.user == asset.ownerAccount and request.user == asset.holderAccount:
                    if newStatus == 'OPEN' or newStatus == 'CLOSED':
                        if asset.status == 'OPEN' or asset.status == 'CLOSED':
                            asset.status = newStatus
                        else:
                            return Response({"reason": "INVALID_STATE", "message": "Asset status cannot be changed at this point"}, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        return Response(status=status.HTTP_400_BAD_REQUEST)

                else:
                    return Response({"reason": "NOT_ALLOWED"}, status=status.HTTP_400_BAD_REQUEST)
        except Asset.DoesNotExist:
            raise Http404
        asset.save()
        serializer = AssetSerializer(asset)
        return Response(serializer.data, status=status.HTTP_200_OK)

def root(request):

    return render(request, 'root.html', {})

# from django.shortcuts import render

# Create your views here.

def item_list(request):
    """ List all Items """

    items = Item.objects.all()
    serializer = ItemSerializer(items, many=True)
    return JsonResponse(serializer.data, safe=False)


def item_detail(requests, pk):
    """ Retrieve an Item"""

    try:
        item = Item.objects.get(pk=pk)
    except Item.DoesNotExist:
        return HttpResponse(status=404)

    serializer = ItemSerializer(item)
    return JsonResponse(serializer.data)


@csrf_exempt
def assets_handler(request):
    if request.method == 'GET':
        return asset_list(request)
    else:
        if request.method == 'POST':
            return create_asset(request)


def asset_list(request):
    assets = Asset.objects.all()
    serializer = AssetSerializer(assets, many=True)
    return JsonResponse(serializer.data, safe=False)


def create_asset(request):
    name = request.GET.get('name')
    status = request.GET.get('status')
    loggedUser = get_logged_user()
    newAsset = Asset.objects.create(name=name, status=status, ownerAccount=loggedUser, holderAccount=loggedUser)
    serializer = AssetSerializer(newAsset, many=False)
    return JsonResponse(serializer.data, safe=False)
    # return HttpResponse('created asset: {0} with status {1}'.format(name, status))


def asset_detail(request, id):
    return JsonResponse('asdfasdf'+id, safe=False)


def user_requests(request, username):
    return JsonResponse("request targeted on user " + username, safe=False);

# temporary implementation - will oberride this with some value from the session, basic auth, scrf etc.
def get_logged_user():
    loggedUser = User.objects.get(pk=1)
    logger.error('retrieving user: %s', loggedUser)
    return loggedUser

def echo(request):
    logger.error('In echo()')
    return HttpResponse("echo")

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

class AssetViewSet(viewsets.ModelViewSet):
    queryset = Asset.objects.all()
    serializer_class = AssetSerializer