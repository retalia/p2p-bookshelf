
#
# [options='header']
# |======
# |requestId-Id (pk) |
# |requestId         |
# |=====
#
# RequestApproval table helps ensure a request is approved as an atomic operation so that in case both ownerApproval and holderApproval are set to YES/NO the necessary actions are taken. As primary a pseudo composite key is used that consists from the updated request id concatenated with a manually increased value (per-requestId). At the same time a foreign key to the request table is used to allow easy retrieval.
#
# When a user wants to update one of request's ownerApprove or holderApprove fields the followig algorithm is used:
# 1. All (if any) existing request approval records are loaded. The most recent is kept
# 3. The id of the requestApproval is extracted. If there were  no records we start from '1'
# 2. The request to be updated is loaded
# 4. A RequestApproval request is added with increased Id. If someone else already added the record this will fail.
# 5. In case we failed we go to (1)
# 6. Otherwise we process the request and update it.
#
#
# class Atomic:
#
#     def __init__(self, tableName, operationName, manager):
#         self.tableName = tableName
#         self.operationName = operationName
#         self.manager = manager
#         self.lockerIndex
#
#     def start(self, criticalRecordId): # criticalRecordId would be requestId
#         self.lockerIndex = 0 # assume this is the first locker record
#         lastLockerRecord = manager.get(criticalRecordId) # retrieve all records in requestApprovals for the specific request. Keep latest only
#         if lastsLockerRecord not nil:
#             self.lockerIndex = extractIndexFromComposite( lastLockerRecord.compositeId )
#         return
#
#     def tryToWrite(self):
#         newIndex = self.lockerIndex + 1
#         lastLockerRecord.compositeId = buildCompositeFromIndex(newIndex)
#         try:
#             lastLockerRecord.save(lastLockerRecord)
#         except: # only catch duplicate primary key
#             return false
#
#     def extractIndexFromComposite(self, compositeId): # extracts the index part from a composite key like 12345-0 12345-1 etc.
#         index = ...
#         return index
#
#     def buildCompositeFromIndex(self, index):
#         composite = id + index
