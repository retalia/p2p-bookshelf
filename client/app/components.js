angular.module('bookshelf').component('home', {
    templateUrl: 'static/templates/home.html',
    bindings: {
        items: '<'
    }
})
.component('itemListView', {
    templateUrl: "static/templates/itemListView.html",
    controller: function (AssetResource) {
        this.assetResponse = AssetResource.query();
    }
})
.component('itemDetails', {
    templateUrl: 'static/templates/itemDetails.html',
    controller: function (AssetResource, $stateParams) {
        this.asset = AssetResource.get({id:$stateParams.id});
    }

})
.component('loginView', {
    templateUrl: 'static/templates/loginView.html',
    bindings: '<'
})
.component('newItemView', {
    templateUrl: 'static/templates/newItemView.html',
    controller: function (AssetResource, Notifications ) {
        this.newItem = {};

        this.doCreateItem = function(item) {
            AssetResource.save(item, function (data) {
                Notifications.success("Item saved");
            }, function () {
                Notifications.error("Error saving item");
            });
        }
    }
})
;


angular.module('bookshelf').component('notificationAlerter', {
    templateUrl: 'static/templates/notificationAlerter.html',
    controller:  function ($scope, $timeout) {
        var alerts = []
        this.alerts = alerts;

        $scope.$on("notification-event", function (eventName, data) {
            console.log("received notification event");
            alerts.push(data);
            $timeout(function () {
                if (alerts.indexOf(data) != -1)
                    alerts.splice(alerts.indexOf(data),1);
            }, 3000)

        });
    }
});



