// this is app.js that contains nothing for the time being

var myApp = angular.module('bookshelf', [
    'ui.router',
    'ngResource',
    'ngCookies'
]);

myApp.config(function($stateProvider, $httpProvider) {

  $httpProvider.defaults.xsrfCookieName = 'csrftoken';
  $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

  var helloState = {
    name: 'hello',
    url: '/hello',
    template: '<h3>hello world!</h3>'
  }

  var aboutState = {
    name: 'about',
    url: '/about',
    template: '<h3>Its the UI-Router hello world app!</h3>'
  }

  var itemsState = {
    name: 'itemList',
    url: '/items',
    component: 'itemListView'
  }

  var homeState = {
    name: 'home',
    url: '/home',
    component: 'home',
    resolve: {
        items: function (ItemsService) {
            return ItemsService.getItems();
        }
    }
  }

  var itemDetailsState = {
    name: 'itemDetails',
    url: '/items/{id}',
    component: 'itemDetails',
  }

  $stateProvider.state(helloState);
  $stateProvider.state(aboutState);
  $stateProvider.state(itemsState)
  $stateProvider.state(homeState);

  $stateProvider.state({
    name: 'newItem',
    url: '/items/new',
    component: 'newItemView'
  });

  $stateProvider.state(itemDetailsState);



});

//myApp.run(function ($trace) { $trace.enable()});

