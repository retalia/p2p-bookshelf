angular.module('bookshelf').factory('ItemsService', function ($q) {
    console.log('Created ItemsService');

    return {
        getItems:  function () {
            var deferred = $q.defer();
            deferred.resolve(['item1', 'item11', 'item111']);
            return deferred.promise;
        },
        getOne: function () {
            var deferred = $q.defer();
            deferred.resolve({id:1, title:'first item'});
            return deferred.promise;
        }
    }
});

angular.module('bookshelf').factory('AssetResource', function ($resource) {
    //return {}
    return $resource("api/assets/:id");
});

// broadcasts custom event objects that are caught by the notificationsCtrl and handled accordingly (an alert is displayed typically)
angular.module('bookshelf').factory('Notifications', function ($rootScope) {
    return {
        success: function (message) {
            $rootScope.$broadcast('notification-event', {message: message,type:'success'});
        },
        error: function (message) {
            $rootScope.$broadcast('notification-event', {message: message ,type:'error'});
        }

    }
})