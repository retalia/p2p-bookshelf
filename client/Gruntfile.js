//Grunt is just JavaScript running in node, after all...
module.exports = function(grunt) {

  // All upfront config goes in a massive nested object.
  grunt.initConfig({
    // You can set arbitrary key-value pairs.
    distFolder: '../../restapi/static/js',
    // You can also set the value of a key as parsed JSON.
    // Allows us to reference properties we declared in package.json.
    pkg: grunt.file.readJSON('package.json'),
    // Grunt tasks are associated with specific properties.
    // these names generally match their npm package name.
    concat: {
      // Specify some options, usually specific to each plugin.
      options: {
        // Specifies string to be inserted between concatenated files.
        separator: ';'
      },
      // 'dist' is what is called a "target."
      // It's a way of specifying different sub-tasks or modes.
      dist: {
        // The files to concatenate:
        // Notice the wildcard, which is automatically expanded.
        src: ['app/*.js'],
        // The destination file:
        // Notice the angle-bracketed ERB-like templating,
        // which allows you to reference other properties.
        // This is equivalent to 'dist/main.js'.
        dest: '<%= distFolder %>/main.js'
        // You can reference any grunt config property you want.
        // Ex: '<%= concat.options.separator %>' instead of ';'
      }
    },
    copy: {
        // bookshelf application files that need to be updated regularly
        app: {
            files: [
              //https://github.com/gruntjs/grunt-contrib-copy
              {expand: true, cwd: "app", src: "**", dest: '../restapi/static/js/', filter: ''},
              {expand: true, cwd: "css", src: "**", dest: '../restapi/static/css/', filter: ''},
            ],
        },
        // third party js library files that do not need to be updated regularly
        lib: {
            files: [
                {expand: true, cwd: "node_modules/angular", src: "angular.js", dest: '../restapi/static/js/lib', filter: 'isFile'},
                {expand: true, cwd: "node_modules/angular-resource", src: "angular-resource.js", dest: '../restapi/static/js/lib', filter: 'isFile'},
                {expand: true, cwd: "node_modules/angular-cookies", src: "angular-cookies.js", dest: '../restapi/static/js/lib', filter: 'isFile'},
                {expand: true, cwd: "node_modules/@uirouter/angularjs/release", src: "angular-ui-router.js*", dest: '../restapi/static/js/lib', filter: 'isFile'},
                {expand: true, cwd: "node_modules/bootstrap/dist/js", src: "bootstrap.min.js*", dest: '../restapi/static/js/lib', filter: 'isFile'},
                {expand: true, cwd: "node_modules/jquery/dist", src: "jquery.min.js", dest: '../restapi/static/js/lib', filter: 'isFile'},
                {expand: true, cwd: "node_modules/popper.js/dist", src: "popper.min.js", dest: '../restapi/static/js/lib', filter: 'isFile'},
                {expand: true, cwd: "node_modules/bootstrap/dist/css", src: "bootstrap.min.css", dest: '../restapi/static/css', filter: 'isFile'},
                {expand: true, cwd: "node_modules/bootstrap/dist/css", src: "bootstrap-theme.min.css", dest: '../restapi/static/css', filter: 'isFile'},
            ]
        }
    },
    watch: {
      scripts: {
        files: ['app/**/*.js','css/**/*.css'],
        tasks: ['copy:app'],
        options: {
          spawn: false,
        },
      },
    },
  }); // The end of grunt.initConfig

  // We've set up each task's configuration.
  // Now actually load the tasks.
  // This will do a lookup similar to node's require() function.
  //grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Register our own custom task alias.
  //grunt.registerTask('build', ['concat']);
  grunt.registerTask('build', ['copy']);
};
